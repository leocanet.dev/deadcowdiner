// Déclaration des variables pour initialiser le jour, l'heure et les minutes.
// Weekdays est un tableau qui contient les horaires du restaurant selon les jours de la semaine
let d = new Date();
let n = d.getDay();
let now = d.getHours() + "." + d.getMinutes();
let weekdays = [
        ["Sunday", 12.00, 14.00, 19.00,22.00],
        ["Monday"], // Restaurant fermé le lundi
        ["Tuesday", 12.00, 14.00, 19.00,22.00],
        ["Wednesday", 12.00, 14.00, 19.00,22.00],
        ["Thursday", 12.00, 14.00, 19.00,22.00],
        ["Friday", 12.00, 14.00, 19.00,22.00],
        ["Saturday", 12.00, 14.00, 19.00,22.00]
    ];
    let day = weekdays[n];

// Verifie la date et l'heure selon les horaires du restaurant
    if (now > day[1] && now < day[2] || now > day[3] && now < day[4]) {
  document.getElementById("currently_open").innerHTML=" Restaurant Ouvert";
  document.getElementById("open").classList.remove("close");
    }
     else {
  document.getElementById("currently_open").innerHTML=" Restaurant Fermé";
  document.getElementById("open").classList.add("close");
  document.getElementById("currently_open").classList.add("currently_closed");
    };

// Fonction qui vient ajouter une classe responsive au nav
function myFunction(){
    let x = document.getElementById("navbar");
    if (x.className === "nav"){
        x.className += " responsive";
    } else {
        x.className = "nav"
    }
};

