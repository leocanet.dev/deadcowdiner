"use strict";

const modal = document.querySelector(".modal");
const modaldiv = document.querySelector("#modaldiv");
const btnCloseModal = document.querySelector("#close-modal");
const btnsOpenModal = document.querySelectorAll(".show-modal");

const imageModal = document.getElementById('imageModal')

const openModal = function (img_url) {
    imageModal.src = img_url;
    modal.classList.remove("hidden");
    modaldiv.classList.remove("hidden");
};

const closeModal = function () {
    modal.classList.add("hidden");
    modaldiv.classList.add("hidden");
};

btnCloseModal.addEventListener("click", closeModal);
modaldiv.addEventListener("click", closeModal);